#!/usr/bin/python3
import socket, sys, pickle
from constante import *

if len(sys.argv) < 2 :
    print('Missing port number')
    exit(1)

HOST = 'localhost'
PORT = int(sys.argv[1])
s= socket.socket(socket.AF_INET6 , socket.SOCK_STREAM , 0)
s.setsockopt(socket.SOL_SOCKET , socket.SO_REUSEADDR , 1)
s.connect((HOST, PORT))

player=None

while True:
    data= s.recv(1500)
    if not len(data):
        break
    data = pickle.loads(data)
    #(Protocole,data)
    # recupere le joueur 
    if data[0] == PLAYER:
        player = data[1]
    elif data[0]==GRID :
        cells=data[1]
        print("-------------")
        for i in range(3):
            print("|",symbols[cells[i*3]], "|",  symbols[cells[i*3+1]], "|",  symbols[cells[i*3+2]], "|");
            print("-------------")
    elif data[0] == QUESTION:
        shot = -1
        while shot <0 or shot >=NB_CELLS:
            shot =int (input ("quel case allez-vous jouer ? "))
        s.sendall(pickle.dumps(shot))
    elif data[0] == GAMEOVER :
        if player == data[1]:
            print('You win')
        
        else :
            print('You loose')
        #Voulez-vous continuez ? (O/N) :
        demande = None
        ANSWER=["N","n","O","o"]
        while demande not in ANSWER: 
            demande = input("Voulez-vous continuez ? (O/N)")
        # Non 
        if demande == 'N' or demande == 'n' :
            exit(0)

   #Autre problreme si le deuxieme joueur ne veut plus jouer 
   # l'autre joueur continue a jouer. 
