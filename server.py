#!/usr/bin/python3
import socket, select, pickle
from grid import *
from constante import *

s = socket.socket(socket.AF_INET6 , socket.SOCK_STREAM , 0)
s.setsockopt (socket.SOL_SOCKET , socket.SO_REUSEADDR , 1)
s.bind(('localhost',7777))
s.listen(2)

players=[]



def recv(ss):
	msg = ss . recv (1500)
	if len ( msg ) == 0:
		print ( "client disconnected")
		ss.close()
		players.remove (ss)
	return msg

while True :
	while len(players) < 2 : 
		listL, listw,error = select.select(players+[s],[],[])
		for ss in listL:
			if s==ss:
				sc , a = s.accept ()
				print ("new client : ", a )	
				sc.sendall(pickle.dumps([PLAYER,len(players)+1]))	
				players.append(sc)
					
	grids = [grid(), grid(), grid()]
	current_player = J1
	
	while grids[0].gameOver() == -1:
		#envoie de gri et de data 
		# Probleme d'encodage remplasser par pickle loads pour decoder et dumps pour encoder 
		players[current_player-1].sendall(pickle.dumps([GRID,grids[current_player].cells]))
		shot = -1
		while shot <0 or shot >=NB_CELLS:
			players[current_player -1].sendall(pickle.dumps([QUESTION]))
			# ça pourait planter le programme acause int 
			shot =int( pickle.loads(recv(players[current_player -1])) )
			
		if grids[0].cells[shot] != EMPTY:
			grids[current_player].cells[shot] = grids[0].cells[shot]
		else:
			# si le joueur essaye de jouer sur la mm case que l'autre
			grids[current_player].cells[shot] = current_player
			grids[0].play(current_player, shot)
			current_player = current_player%2+1
	for ss in players:
		ss.sendall(pickle.dumps([GAMEOVER,grids[0].gameOver()]))
s.close ()


